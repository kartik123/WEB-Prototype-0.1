'use strict';
// var firebase = require('firebase');
var app = angular.module("myApp", ['ui.router', 'ngMaterial']);



// let self = this;
// var ref = firebase.database().ref();
// var ref2 = firebase.storage().ref();
// // console.log(ref);
// var usersRef = ref.child('recording').child('id1');
// usersRef.on("value",
//     function(snapshot) {
//         var highlights = snapshot.val().highlights;
//         var notes = snapshot.val().notes;
//         // console.log("highlights", highlights);
//         // console.log("notes", notes);
//         // angular.forEach(highlights, function(value, key) {
//         //     console.log("Starttime", value.starttime, "EndTime", value.endtime);
//         // })
//         // angular.forEach(notes, function(value, key) {
//         //     console.log("noteAddedTime", value.noteAddedTime, "notetext", value.notetext);
//         // })

//     })
app.service('fservice', function() {
    var myfunctions = {};
    myfunctions.signin = function(email, password) {
        firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(email);
            console.log(password);

        });

    }

    myfunctions.create = function(email, password) {

        firebase.auth().createUserWithEmailAndPassword(email, password)
            .catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                if (errorCode == 'auth/weak-password') {
                    alert('The password is too weak.');
                } else {
                    alert(errorMessage);
                }
                console.log(error);
            });


    }
    myfunctions.uid = function() {
        console.log("hello kartik");
        firebase.auth().onAuthStateChanged(firebaseUser => {
            if (firebaseUser) {
                return firebaseUser.uid;
            } else {
                console.log("ERROR,ERROR ERROR")
            }

        })

    }
    myfunctions.currentuid = function() {
        var user = firebase.auth().currentUser;
        return user.uid;
    }
    myfunctions.signout = function() {
        firebase.auth().signOut().then(function() {
            console.log('Signed Out');
        }, function(error) {
            console.error('Sign Out Error', error);
        });
    }


    return myfunctions;

});

app.controller("SampleCtrl", ["$scope", "$timeout", "fservice", "$state", "$mdBottomSheet",

    function($scope, $timeout, fservice, $state, $mdBottomSheet, ) {


        $scope.afunction = function a() {
            console.log("ja raha hai")
            console.log($scope.email);
            fservice.signin($scope.email, $scope.password);
            $state.go('home');
            // $rootScope.currentuid = fservice.currentuid();
        }

        $scope.sout = function() {
            fservice.signout();
            $state.go('login');
        }
        $scope.showGridBottomSheet = function() {
            $scope.alert = '';
            $mdBottomSheet.show({
                templateUrl: 'bottom.html',
                controller: 'SampleCtrl',
                clickOutsideToClose: true
            }).then(function(clickedItem) {
                $mdToast.show(
                    $mdToast.simple()
                    .textContent(clickedItem['name'] + ' clicked!')
                    .position('top right')
                    .hideDelay(1500)
                );
            }).catch(function(error) {
                // User clicked outside or hit escape
            });
        };


        $scope.music = function() {
            $scope.alert = '';
            $mdBottomSheet.show({
                templateUrl: 'music.html',
                controller: 'SampleCtrl',
                clickOutsideToClose: false
            }).then(function(clickedItem) {
                $mdToast.show(
                    $mdToast.simple()
                    .textContent(clickedItem['name'] + ' clicked!')
                    .position('top right')
                    .hideDelay(1500)
                );
            }).catch(function(error) {
                // User clicked outside or hit escape
            });
        };

    }

]);
app.controller("SampleCtrl2", ["$scope", "$timeout", "fservice",
    function($scope, $timeout, fservice) {



    }

]);
app.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: '/home.html',
                controller: 'SampleCtrl'
            })
            .state('login', {
                url: '',
                templateUrl: '/login.html',
                controller: 'SampleCtrl'


            })
            .state('yz', {
                url: '',
                templateUrl: '/play.html',
                controller: 'SampleCtrl2'


            });
    }
]);